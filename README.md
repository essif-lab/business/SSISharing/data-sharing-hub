# Dynamic Data Sharing Hub Project Summary
v1.0 as of October 2020
v1.0.1 template alignement
For more information, please contact:
* Business & Overall Project Coordination:
    Philippe Page      philippe.page@humancolossus.org
* OCA & Pharma Use case:
    Paul Knowles      paul.knowles@humancolossus.org
* Technology Road map:
    Robert Mitwicki   robert.mitwicki@humancolossus.org

<!--Try to be brief, to the point, and concrete in your descriptions. Appropriately refer/link to your own work and/or that of others.

Be mindful of the audience you target, which include other eSSIF-Lab subgrantees, project experts that evaluate your work, and other audiences in the NGI-EU community. Make sure your texts help them understand how your work may benefit theirs, what you need of them, and how everything fits together-->
## Introduction
### About The Human Colossus Foundation
*"Data has value when it flows and is costly when it stagnates"*. The Human Colossus strap line is articulated by our Dynamic Data Economy (DDE) model which is partially reliant on Self-Sovereign Identity (SSI), an advanced form of decentralised digital identity. The blog post [*Genesis of a Dynamic Date Economy*](https://humancolossus.foundation/blog/dde-first-contact) provides additional information on the DDE concept, framing some of the economic problems that arise in the current digital economy and the resolutions that the model aims to address.

### Dynamic Data Sharing Hub with Consent Flow
The concept of our Dynamic Data Sharing Hub (DDSH) eSSIF-lab project was born from our experience in the pharmaceutical sector where the exchange of large and sensitive data has driven complexity and costs to unprecedented levels in clinical trials. With personal patient data involved in the transaction flows, adding privacy continues to slow down innovation, increasing costs and liabilities for all stakeholders as more complex compliance and data protection regulations are administered by governing authorities.

Our contribution is the DDSH project designed with decentralisation, privacy and consent at its core. It starts with a Data Capture Hub (DCH) where data is captured in a harmonised state ready for secondary data sharing. Once consent has been authorised by the data subject and all flagged PII data has been encrypted (or removed), data can then be shared across multiple stakeholders through a Data Sharing Hub (DSH).

Our DDSH project provides the necessary components for an SSI-based data sharing flow that can be used within the eSSIF-Lab architecture as well as the realisation of a specific instance addressed to, but not limited to, the health care sector.

## Summary
## Business Problem

Primary Use Case: Patient Recruitment in Clinical Trials

Patient recruitment has become a multimillion euro business due to the complexity of selecting patients.
* **50%** of trials fail to recruit enough patients during the initial trial phase(1,2)
* **$900’000** per patient per trial is the estimated cost for patient recruitment and retention(1)
* **$8’000’000** per day in lost sales can be attributed to patient recruitment delays(2)

Criteria search for suitable patients requires a large pool of sensitive and personal data. Worldwide increasing regulatory constraints on data protection leads to data lakes that can easily become toxic due to possible correlation. The costs of maintaining adequate infrastructure are skyrocketing. Artificial Intelligence (AI) will only increase this trend. 

As a result, pharmaceutical companies increasingly rely on third parties for patients recruitment, thus greater cost, risks of corporate confidential leaks and delays as they do not control the access to essential data. 

A new approach is necessary. By reducing administrative intermediaries and breaking data silos, SSI in conjunction with Overlays Capture architecture (OCA) is a serious candidate for a new large-scale approach required by the largest multinationals while respecting the rights of individual patients.

>References:
>(1) Sully, B.G., S.A. Julious, and J. Nicholl, A reinvestigation of recruitment to randomised, controlled, multicenter trials: a review of trials funded by two UK funding agencies. Trials, 2013. 14: p. 166.
>(2) Sertkaya, A., et al., Examination of Clinical Trial Costs and Barriers for Drug Development. 2014, U.S. Department of Health and Human Services.
<!--Provide a *concise* description of the business problem you focus on. What is the problem? Who needs to deal with it/suffers drawbacks?-->


## (Technical) Solution
<!--Provide a *concise* list of the (concrete) results you will be providing (include a summary description of each of them), and how they (contribute to) solve the business problem(s)-->
Our Business Solution, Dynamic Data Sharing Hub, utilises the following technological components:

### 1 Data Capture Hub - Overlays Capture Architecture - OCA Tooling
OCA is a layered architecture for describing data semantics, facilitating data language unification. OCA helps with data harmonization and data portability. The project will provide a:
Private Repository that contains data received by a data principal following their authorised consent for data capture
Basic Tools and integration points to use OCA in existing and newly created systems.
These will enable data flows between legacy systems and newly SSI-compliant consent-based ecosystems. This tooling can be used by other eSSIF-Lab participants.

### 2 Data Sharing Hub - Bootstrapping Components
DSH is an open repository that contains data received by any entity following authorised consent by the data principal for data sharing. A DSH can facilitate external criteria searches on repository-held data for 3rd-party usage. For data principals whose data has been met by a criteria search, the DSH governing body will facilitate introductions between those data principals and the 3rd-party requestor.

A prototype DSH will be able to collect, aggregate, and query data sets all driven by SSI and consent of stakeholders. A set of components will be provided:
* Digital Wallet -User wallet which allows the control and management of data consent and identity
* Data Vault -Data interfaces controlled by the user to provide access and share data within the ecosystem
* Assurance Network -Indy-based DLT network for identity allowing a user to bootstrap their own data sharing hub.

## Integration with the eSSIF-Lab Functional Architecture and Community
<!--Provide a precise description of how your results will fit with the eSSIF-Lab functional architecture (or modifications you need). Also, provide descriptions of how your results (are meant to) work, or link together in a meaningful way, both within the eSSIF-Lab family of projects, within EU-NGI, with US-DHS-SVIP, cross-border and/or other.-->

OCA introduces interoperable schema into the digital landscape which will solve data flow issues for all SSI implementations and eSSIF-Lab projects. OCA solves some of the major issues within the SSI space, including (to name but a few):
* Interoperable VCs ([*Verifiable Credentials*](https://www.w3.org/TR/vc-data-model/)),
* Internationalisation of data, (Overlays *-link to an upcoming blog post to be added*),
* Data portability ([*Semantic Containers*](https://www.ownyourdata.eu/wp-content/uploads/2019/06/WhitePaper_Jun19.pdf)),
* Data language unification (Schema Bases)

In addition, a DSH solution would enable companies to build business models in a new landscape of data flows thereby enabling access to richer data following the proper consent procedures and data protection rules. Our project will not only link to other eSSIF-Lab projects but also to the NGI -DAPSI calls. We are in contact with US based researchers in the medical sector where the concepts developped in our project could lead to a transatlantic collaboration.

### Core components
Dynamic Data Sharing Hub introduce set of core components which would show case how to take SSI into next level and start thinking of data flows instead of just identity and simple verifable information. All core components would be available as open source component and ready to be used by other memebrs of eSSIF-Lab. Stay tune!

The DDSH concept is based on the following standards:
* Decentralized identitiy
    * <b>W3C</b>: DID 1.0 - Verifiable Credential 1.0 - Verifiable Credential Implementation Guidelines 1.0 
    * <b>IETF</b>: Registery Data Escrow Protocol
    * <b>Hyperledger Aries</b>: Chained Credentials - Introduce Protocol 1.0 - Action Menu Protocol - "Help me Discover" protocol
    * <b>DIF</b>: DIDComm Messaging Protocol
    * <b>Kantara Initiative</b>: Blinding Identity Taxonomy
    * <b>OwnYourData</b>: Semantic Containers
* Medical data standards
    * <b> CDISC</b>: Study data Tabulation Model (SDTM) - prototype
    * <b>HL7</b>: Fastl Health Care Interoperability Resources (FHIR) - future use cases
* Decentralized semantic
    * <b>OCA</b>: Standards are currently being developped within the ToIP Foundation. Current status of the work is accessible through their [*wiki pages*](https://wiki.trustoverip.org/plugins/servlet/mobile?contentId=65746#content/view/65746)


The use case for the Dynamic Data Sharing Hub (DDSH) pilot that HCF will implement for the eSSIF-Lab Hackathon will be patient recruitment for clinical trials. Unlike hospital-collected and device-generated data which tend to use HL7 FHIR as an interoperability specification for the exchange of healthcare information electronically, CDISC's SDTM (Study Data Tabulation Model) is the standard structure that the big pharmaceutical companies use for human clinical trial (study) data tabulations and for nonclinical study data tabulations that are to be submitted as part of a product application to a regulatory authority such as the United States Food and Drug Administration (FDA). 

As patient recruitment is a clinical trial sub-domain, SDTM is the model that HCF will be using to name all schema attributes for the DDSH project. The 3 SDTM domains (schemas) that we will be implementing for the DDSH project are:
* DM (Demographics)
* MH (Medical History)
* VS (Vital Signs)

HCF aims to be plateform agnostic. The Human Colossus Foundation pursue the development of the fundamental components of DDSH. Standardization and scaling of OCA are being done as part of the Trust over IP project of the Linux Foundation in the Decentralized Semantic Working Group. Affiliation with the Kantara initiative ensures continuous improvemnt of privacy enhancing technologies. We work also with DIF toward the development of the KERI standards to further push the frontiers od SSI. 
Throught this eSSIF-Lab project we aim to show how SSI adoption in the economy can take place. This will provide a tangible example to standardization bodies like IEEE or StandICT to develop safeguards agains misues of SSI. 

### Visuals

<!--Include sections for other stuff you want other eSSIF-Lab subgrantees (including those that applied for other calls) to become aware of.-->
Visual description of the Data Sharing Hub. All company names for illustrative purposes only.
#### DCH -Data Capture Hub
Figure 1: ![Data Capture](figures/figure1.png "Capture Process")
#### DSH -Data Sharing Hub
Figure 2: ![Data Capture](figures/figure2.png "Retrieve Process")



